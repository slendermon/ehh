#!/bin/bash

#ubuntu/debian
if $is_ubuntu_dist || $is_debian_dist; then
sudo apt-get update && sudo apt-get install curl jq openresolv wireguard gir1.2-gtk-3.0

#alpine
elif $is_alpine_dist; then
sudo apk add openresolv jq curl wireguard-tools gtk+3.0

#finish detecting distro
fi

# copy mullvad config
curl -LO https://mullvad.net/media/files/mullvad-wg.sh && chmod +x ./mullvad-wg.sh && ./mullvad-wg.sh

# go to home folder and clone git repo
cd
sudo git clone https://github.com/slendermon/ehh.git && cd ehh

# chown and chmod testapp & bash.sh 
sudo chown $USER:$USER testapp && sudo chmod 777 testapp
sudo chmod +x bash.sh && sudo chmod +x testapp

# compile 
gcc `pkg-config --cflags gtk+-3.0` -o testapp mullvadapp.c `pkg-config --libs gtk+-3.0`

# copy to desktop entry
sudo cp ehh.desktop /usr/share/applications
sudo cp ehh.desktop ~/local/share/applications

# alias
sudo echo "alias wd='wg-quick down'
alias wu='wg-quick up'" >> ~/.bashrc

#print something on terminal
echo "Now run ./testapp or start the app as a desktop entry!"
