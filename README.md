# ehh
Wireguard application gui for mullvad 

# Install
Auto-install(don't know if it works):
```wget https://github.com/slendermon/ehh/blob/main/ehh.sh && chmod +x ehh.sh && ./ehh.sh```

Dependencies for debian/ubuntu:\
`sudo apt-get update && sudo apt-get install curl jq openresolv wireguard gir1.2-gtk-3.0`

Dependencies for alpine:\
`sudo apk add openresolv jq curl wireguard-tools gtk+3.0`

Config Files, clone repository, run gui:\
`curl -LO https://mullvad.net/media/files/mullvad-wg.sh && chmod +x ./mullvad-wg.sh && ./mullvad-wg.sh`

``` cd ```\
``` git clone https://github.com/slendermon/ehh.git && cd ehh ```\
``` gcc `pkg-config --cflags gtk+-3.0` -o testapp mullvadapp.c `pkg-config --libs gtk+-3.0` ```\
`./testapp`

----------

If you want a desktop entry, put the file `ehh.desktop` in `/usr/share/applications/`:\
`sudo cp ehh.desktop /usr/share/applications`\
`cp ehh.desktop ~/.local/share/applications`

# how to make gtk
https://docs.gtk.org/gtk3/getting_started.html

# About
here's mullvad's guide just in case:\
```https://mullvad.net/en/help/wireguard-and-mullvad-vpn/```

if wanted, you can change mullvad to another wireguard connection vpn. 
